Returns all possible words that can be created using the provided letters.

Created for use with this online game:
http://www.addictinggames.com/puzzle-games/whizzwordz.jsp
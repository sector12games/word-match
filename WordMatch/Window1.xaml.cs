﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Threading;
using System.Windows.Threading;

namespace WordMatch
{
    /// <summary>
    /// This application was originally designed for use with
    /// "Whiz Words", a flash game found online at AddictingGames.
    /// Here's the URL as of 8-28-2011:
    /// http://www.addictinggames.com/puzzle-games/whizzwordz.jsp
    /// 
    /// You can play other word scramble games with it, but the 
    /// dictionary might be different.
    /// </summary>
    public partial class Window1 : Window
    {
        // The character offset for ascii values. 'a' starts at 97.
        // We'll use this for counting how many of each letter of 
        // the alpahbet we have.
        public static int CHARACTER_OFFSET = 97;

        public Window1()
        {
            InitializeComponent();

            this.PopulateDictionary();
        }

        public void PopulateDictionary()
        {
            StreamReader reader;
            string delimiter = "\", ";
            string s;
            string filename = @"..\..\dictionary.txt";
            reader = File.OpenText(filename);

            // the dictionary is one long string - no character breaks
            s = reader.ReadLine();

            // close the reader
            reader.Close();

            // split the long string into words
            m_words = s.Split (delimiter.ToCharArray (), StringSplitOptions.RemoveEmptyEntries).ToList ();
        }

        private void Solve()
        {
            List<string> matches = new List<string>();
            bool matchFound;

            // build a 26 slot array, to hold the count for
            // each letter of the alphabet
            int [] letterCountArray = new int [26];
            foreach (char c in m_characters)
            {
                // loop through all the characters the user specified,
                // to keep track of how many of each letter there are
                letterCountArray [c - CHARACTER_OFFSET]++;
            }

            // check each word in our dictionary to see if it
            // can be formed using the letters the user specified
            foreach (string word in m_words)
            {
                // gracefully exit the thread if the user pressed stop
                if (!m_isRunning)
                    break;

                // keep a bool for determining if this dictinoary word is a match
                matchFound = true;

                // check each letter, to see if it is in the
                // list of characters the user specified
                foreach (char c in word)
                {
                    // keep in mind we need to keep track of counts
                    // too, because there can be duplicates in the
                    // characters the user inputs
                    int charCount = this.CountCharacters(c, word);

                    // We found a letter in our dictionary word that wasn't
                    // in the list of characters given to us by the user,
                    // or there was too many of the letter in the dictionary word.
                    // This word should not be included in the results,
                    // move onto the next.
                    if (letterCountArray[c - CHARACTER_OFFSET] < charCount)
                    {
                        matchFound = false;
                        break;
                    }    
                }

                // if this dictionary word was a match, update the results
                // window, but do it in the main UI thread
                if (matchFound)
                {
                    matches.Add(word);
                }
            }

            // sort the matches by string length
            m_matches = this.SortResults(matches);

            // update the buttons now that our work is done
            this.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                (Action)delegate
            {
                this.UpdateResults();
                this.startButton.IsEnabled = true;
                this.stopButton.IsEnabled = false;
            });
        }

        private List<string> SortResults(List<string> matches)
        {
            List<string> results = new List<string>();
            for (int i = 8; i >= 3; i--)
            {
                //results.Add("===" + i + " letter words ===");
                foreach (string match in matches)
                {
                    if (match.Length == i)
                        results.Add(match);
                }
            }
            return results;
        }

        private void UpdateResults()
        {
            StringBuilder builder = new StringBuilder();
            int letterCount = 0;
            foreach (string match in m_matches)
            {
                if (match.Length != letterCount)
                {
                    letterCount = match.Length;
                    builder.Append("=== " + letterCount + " letter words ===");
                    builder.Append("\n");
                }

                builder.Append(match);
                builder.Append("\n");
            }

            this.results.Text = builder.ToString();
        }

        private int CountCharacters(char c, string word)
        {
            int count = 0;

            foreach (char cc in word)
            {
                if (c == cc)
                    count++;
            }

            return count;
        }

        private void StartSendKeys()
        {
            // wait a few seconds before we start sending keys
            Thread.Sleep(3000);

            foreach (string word in m_matches)
            {
                if (!m_sendKeysIsRunning)
                    break;

                //System.Windows.Forms.SendKeys.SendWait (word);
                
                foreach (char c in word)
                {
                    System.Windows.Forms.SendKeys.SendWait(c.ToString ());
                    Thread.Sleep(10);
                }

                System.Windows.Forms.SendKeys.SendWait("{ENTER}");
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            // don't do anything if the user didn't input at least
            // three characters to check
            if (this.characters.Text.Trim().Length < 3)
            {
                MessageBox.Show(this, "You must enter at least 3 characters.");
                return;
            }

            // clear any old results
            this.results.Text = "";

            // get the list of characters the user specified
            string characters = this.characters.Text.Trim().ToLower ();
            m_characters = characters.ToCharArray ().ToList ();

            // disable the start button
            this.startButton.IsEnabled = false;
            this.stopButton.IsEnabled = true;

            // start finding words in a different thread
            m_isRunning = true;
            Thread thread = new Thread(new ThreadStart(this.Solve));
            thread.Start();
        }

        private void Stop_Click (object sender, RoutedEventArgs e)
        {
            // tell the thread to gracefully stop itself
            m_isRunning = false;
        }

        private void SendKeys_Click(object sender, RoutedEventArgs e)
        {
            m_sendKeysIsRunning = true;

            // start sending keys in a different thread
            m_isRunning = true;
            Thread thread = new Thread(new ThreadStart(this.StartSendKeys));
            thread.Start();
        }

        private void StopSendKeysButton_Click(object sender, RoutedEventArgs e)
        {
            m_sendKeysIsRunning = false;
        }

        private List<string> m_words;
        private List<char> m_characters;
        private List<string> m_matches;
        private bool m_isRunning;
        private bool m_sendKeysIsRunning;

    }
}
